$(function(){

	var alto = $(window).innerHeight();
	var ancho = $(window).innerWidth();

	if(ancho>500){
		ancho = 500;
	}

	function newUnits(anchoReal,anchoObjetivo){
		var result = {};
		result.scale = anchoObjetivo/anchoReal;
		result.x = (ancho-anchoObjetivo)/2;
		return result;
	}

	//var game = new Phaser.Game(ancho, alto, Phaser.CANVAS, 'game');

	var game =new Phaser.Game({
					width                   : ancho,
					height                  : alto,
					parent                  : document.getElementById("game"),
					type                    : Phaser.AUTO,
					fps                     : {target: 60, min: 45},
					transparent             : true,
					physics: {
				        default: 'arcade',
				        arcade: {
				            // gravity: {
                            //     y:300
                            // },
                            //debug: true
				        }
				    },
					scene : []
				});
		
		game.alto = alto;
		game.ancho = ancho;
		game.newUnits = newUnits;
		game.scene.add('Boot', Juego.Boot);
		game.scene.add('Preloader', Juego.Preloader);
                game.scene.add('Portada', Juego.Portada);
                game.scene.add('Escogepj', Juego.Escogepj);
		game.scene.add('Game', Juego.Game);
		game.scene.start('Boot');

});